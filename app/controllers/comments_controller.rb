class CommentsController < ApplicationController
	
	http_basic_authenticate_with :name => "111", :password => "111", :only => :destroy

	def create
    @post = Post.find(params[:post_id])
    @comment = @post.comments.new(params[:comment])
    if @comment.save
      redirect_to post_path(@post)
    else 
      @post = Post.find(params[:post_id])
      render "posts/show"
    end
  end

  def destroy
    @post = Post.find(params[:post_id])
    @comment = @post.comments.find(params[:id])
    @comment.destroy
    redirect_to post_path(@post)
  end
end
