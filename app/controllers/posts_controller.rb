class PostsController < ApplicationController

	http_basic_authenticate_with :name => "111", :password => "111", :except => [:index, :show, :new, :create]

	def new
		@post = Post.new
	end

	def create
		@post = Post.new(params[:post])
 
		if @post.save
			redirect_to :action => :index
		else
			render 'new'
		end
	end

	def show
		@post = Post.find(params[:id])
		@comment = Comment.new
	end

	def index
		@post = Post.all
		@post = Post.order(:id).page(params[:page]).per(5)
		@post = [] if @post.nil?
		p @post.inspect
	end	

	def edit
		@post = Post.find(params[:id])
	end

	def update
		@post = Post.find(params[:id])

		if @post.update_attributes(params[:post])
			redirect_to :action => :index
		else
			render 'edit'
		end
	end

	def destroy
		@post = Post.find(params[:id])
		@post.destroy
 
		redirect_to posts_path
	end

	private
		def post_params
    		params.require(:post).permit(:title, :text)
		end
end
